-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.51-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table dim-tk-4.barang
CREATE TABLE IF NOT EXISTS `barang` (
  `id` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `harga_beli` double DEFAULT '0',
  `harga_jual` double DEFAULT '0',
  `jumlah` int(11) DEFAULT '0',
  `biaya_pesan` double DEFAULT '0',
  `biaya_simpan` double DEFAULT '0',
  `lead_time` int(11) DEFAULT '0',
  `tanggal_pesan` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table dim-tk-4.barang: ~2 rows (approximately)
/*!40000 ALTER TABLE `barang` DISABLE KEYS */;
INSERT INTO `barang` (`id`, `nama`, `harga_beli`, `harga_jual`, `jumlah`, `biaya_pesan`, `biaya_simpan`, `lead_time`, `tanggal_pesan`) VALUES
	('SKU-0001', 'Samsung Galaxy S6', 6500000, 7500000, 300, 20000, 500, 5, '2022-02-06'),
	('SKU-0002', 'Samsung Galaxy S7', 7500000, 8500000, 50, 25000, 10000, 5, '2022-02-06'),
	('SKU-0004', 'Realme X6', 2500000, 3500000, 50, 20000, 12500, 3, '2022-01-12');
/*!40000 ALTER TABLE `barang` ENABLE KEYS */;

-- Dumping structure for table dim-tk-4.eoq
CREATE TABLE IF NOT EXISTS `eoq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `bulan` int(11) DEFAULT NULL,
  `id_barang` varchar(255) DEFAULT NULL,
  `nama_barang` varchar(255) DEFAULT NULL,
  `harga_beli` double DEFAULT NULL,
  `harga_pesan` double DEFAULT NULL,
  `biaya_pesan` double DEFAULT NULL,
  `biaya_simpan` double DEFAULT NULL,
  `lead_time` int(11) DEFAULT NULL,
  `permintaan` int(11) DEFAULT NULL,
  `eoq` double DEFAULT NULL,
  `rop` double DEFAULT NULL,
  `total_biaya` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table dim-tk-4.eoq: ~0 rows (approximately)
/*!40000 ALTER TABLE `eoq` DISABLE KEYS */;
INSERT INTO `eoq` (`id`, `tanggal`, `bulan`, `id_barang`, `nama_barang`, `harga_beli`, `harga_pesan`, `biaya_pesan`, `biaya_simpan`, `lead_time`, `permintaan`, `eoq`, `rop`, `total_biaya`) VALUES
	(1, '2022-01-10', 1, 'SKU-0002', 'Samsung Galaxy S7', 7500000, 8500000, 25000, 10000, 5, 2500, 71, NULL, NULL),
	(2, '2022-01-10', 1, 'SKU-0004', 'Realme X6', 2500000, 3500000, 20000, 12500, 3, 500, 32, NULL, NULL),
	(3, '2022-02-01', 2, 'SKU-0004', 'Realme X6', 2500000, 3500000, 20000, 12500, 3, 150, 18, NULL, NULL);
/*!40000 ALTER TABLE `eoq` ENABLE KEYS */;

-- Dumping structure for table dim-tk-4.trole
CREATE TABLE IF NOT EXISTS `trole` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table dim-tk-4.trole: ~2 rows (approximately)
/*!40000 ALTER TABLE `trole` DISABLE KEYS */;
INSERT INTO `trole` (`id`, `nama`, `keterangan`) VALUES
	(1, 'ADMIN', 'ADMIN'),
	(2, 'STAFF', 'STAFF');
/*!40000 ALTER TABLE `trole` ENABLE KEYS */;

-- Dumping structure for table dim-tk-4.tuser
CREATE TABLE IF NOT EXISTS `tuser` (
  `username` varchar(255) NOT NULL,
  `password` tinytext,
  `nama_depan` varchar(255) NOT NULL,
  `nama_belakang` varchar(255) DEFAULT NULL,
  `level` int(11) NOT NULL DEFAULT '0',
  `token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`username`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table dim-tk-4.tuser: ~4 rows (approximately)
/*!40000 ALTER TABLE `tuser` DISABLE KEYS */;
INSERT INTO `tuser` (`username`, `password`, `nama_depan`, `nama_belakang`, `level`, `token`) VALUES
	('admin1@tk4.com', '$2y$11$CFl7PgqdVNn7CYhT5NW4XeajA56cJ/eWyRuRmv1B7RXStKBzPeX72', 'Tony', 'Stark', 1, '9bf49913ca856fa95814f39ac6cb7b19ff0cdc0e'),
	('admin2@tk4.com', '$2y$11$CFl7PgqdVNn7CYhT5NW4XeajA56cJ/eWyRuRmv1B7RXStKBzPeX72', 'Kurt', 'Cobain', 1, 'bcfe959458e7bb0f2ee4268e262c9baaa5e387cc'),
	('staff1@tk4.com', '$2y$11$c3CG/84vL8FF3p9ogDYFPeZ89bB5xmi66EjQor.481LG0WHtQ2PHO', 'Billy', 'Joe', 2, 'abb65cd81deb9231f2421b8e7cc75e4de607349a'),
	('staff2@tk4.com', '$2y$11$c3CG/84vL8FF3p9ogDYFPeZ89bB5xmi66EjQor.481LG0WHtQ2PHO', 'Eddie', 'Vedder', 2, NULL);
/*!40000 ALTER TABLE `tuser` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;



/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table dim-tk-4.customer
CREATE TABLE IF NOT EXISTS `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `kota` varchar(255) DEFAULT NULL,
  `provinsi` varchar(50) DEFAULT NULL,
  `telepon` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table dim-tk-4.customer: ~2 rows (approximately)
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` (`id`, `nama`, `alamat`, `kota`, `provinsi`, `telepon`) VALUES
	(1, 'Corey', 'Jalan A.Yani No 123 RT 01', 'Depok City', 'Jawa Barat', '08123456789'),
	(2, 'Jim', 'Pondok Pinang', 'Jakarta Selatan', 'DKI Jakarta', '087869999');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;


-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.51-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table dim-tk-4.supplier
CREATE TABLE IF NOT EXISTS `supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `kota` varchar(255) DEFAULT NULL,
  `provinsi` varchar(50) DEFAULT NULL,
  `telepon` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table dim-tk-4.supplier: ~0 rows (approximately)
/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;
INSERT INTO `supplier` (`id`, `nama`, `alamat`, `kota`, `provinsi`, `telepon`) VALUES
	(1, 'PT. Samsung Indonesia', 'Jalan pelabuhan , tanjung priuk ', 'Jakarta Utara', 'DKI Jakarta', '0892554965'),
	(2, 'PT. Xiaomi Indonesia', 'Jalan Sudirman', 'Jakarta Selatan', 'DKI Jakarta', '087646464'),
	(3, 'PT. Realme Indonesia', 'Jalan Sudirman Kav 1B', 'Jakarta Selatan', 'DKI Jakarta', '0878787878');
/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;


-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.51-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table dim-tk-4.pembelian
CREATE TABLE IF NOT EXISTS `pembelian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `id_supplier` int(11) DEFAULT NULL,
  `id_barang` varchar(50) DEFAULT NULL,
  `jumlah_beli` int(11) DEFAULT NULL,
  `total_harga` double DEFAULT '0',
  `total_stok` double DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table dim-tk-4.pembelian: ~0 rows (approximately)
/*!40000 ALTER TABLE `pembelian` DISABLE KEYS */;
INSERT INTO `pembelian` (`id`, `tanggal`, `id_supplier`, `id_barang`, `jumlah_beli`, `total_harga`, `total_stok`) VALUES
	(1, '2022-01-10', 3, 'SKU-0004', 10, 25000000, 60),
	(3, '2022-01-10', 1, 'SKU-0002', 5, 37500000, 65);
/*!40000 ALTER TABLE `pembelian` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;


-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.51-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table dim-tk-4.penjualan
CREATE TABLE IF NOT EXISTS `penjualan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `id_pelanggan` int(11) DEFAULT NULL,
  `id_barang` varchar(50) DEFAULT NULL,
  `jumlah_beli` int(11) DEFAULT NULL,
  `total_harga` double DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table dim-tk-4.penjualan: ~0 rows (approximately)
/*!40000 ALTER TABLE `penjualan` DISABLE KEYS */;
INSERT INTO `penjualan` (`id`, `tanggal`, `id_pelanggan`, `id_barang`, `jumlah_beli`, `total_harga`) VALUES
	(2, '2022-01-10', 2, 'SKU-0002', 10, 75000000);
/*!40000 ALTER TABLE `penjualan` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
