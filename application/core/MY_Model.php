<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

    /**
     * Holds an array of tables used
     *
     * @var array
     */
    public $tables = array();


    public function __construct()
    {
        parent::__construct();
        $this->CI = get_instance();
        $this->id = 'id';
        $this->order = array($this->id => 'asc');
        $this->order_kode = array('kode' => 'asc');
    }

    public function set_flashdata($value)
    {
        return $this->session->set_flashdata('message', $value);
    }

    /* */
    public function find_all($table_name = null) {
        $query = $this->db->get($table_name);
        return $query->result();
    }

    public function find_all_asc($field ,$table_name = null) {
        $query = $this->db->from($table_name)->order_by($field, 'ASC')->get();
        return $query->result();
    }

    public function find_all_desc($table_name = null) {
        $query = $this->db->from($table_name)->order_by('id', 'DESC')->get();
        return $query->result();
    }

    public function find_last_specific_data_where($table_name, $field, $order_field)
    {
        $query = $this->db->select($field)->order_by($order_field, 'DESC')->get($table_name);
        return $query->row();
    }

    public function find_most_recent_data($table_name,$order_field)
    {
        $query = $this->db->select('*')->order_by($order_field, 'DESC')->limit(1)->get($table_name);
        return $query->row();
    }

    public function find_all_where($table_name = null, $column = array(), $order = array()) {

        foreach ( (array) $column as $key => $value)
        {
            $query = $this->db->where($key, $value);
        }

        foreach( (array) $order as $key => $value)
        {
            $query = $this->db->order_by($key, $value);
        }

        $query = $this->db->get($table_name);

        return $query->result();
    }

    public function find_specific_field_where($table_name = null, $field = null, $condition = array(), $limit = 0)
    {
        $query = $this->db->select($field);

        foreach ( (array) $condition as $key => $value)
        {
            $query = $this->db->where($key, $value);
        }

        if($limit > 0) {
            $query = $this->db->limit($limit);
        }

        $query = $this->db->get($table_name);

        return $query->result();
    }

    public function update_specific_field_where($table_name, $id, $fields = array())
    {

        $datas = array();
        foreach ( (array) $fields as $key => $value)
        {
            $datas[$key] = $value;
        }

        $this->db->where($this->id, $id);
        $this->db->update($table_name, $datas);
    }


    /**
     * Generate find all data group by dengan parameter
     * @param table_name: nama table yang mau di select
     * @param fields    : field apa saja yang mau di select     array('name', 'title')
     * @param where     : kalau ada wherenya                    array('isactive' => 1)
     * @param groupby   : field groupingnya                     array('title')
     */
    protected function find_distinct_group_by_where($table_name = null, $fields = array(), $where = array(), $groupby = array()) {
        $sql = 'SELECT DISTINCT ';
        
        foreach( (array) $fields as $key => $value) 
        {
            if($key == sizeof($fields) - 1)
            {
                //$sql .= 'ANY_VALUE('.$value.') AS '.$value;
                $sql .= $value;
            }
            else
            {
                //$sql .= 'ANY_VALUE('.$value.') AS '.$value.', ';
                $sql .= $value.', ';
            }
        }

        $sql .= ' FROM '. $table_name;

        // check if where is not empty
        if(sizeof($where) > 0) 
        {
            $sql .=' WHERE ';
            foreach($where as $key => $value)
            {
                if($key == sizeof($where) - 1)    // kalau cuma 1 atau data terakhir,
                {                                 //  langsung buat query wherenya
                    $sql .= $key .' = '. $value;
                }
                else
                {
                    $sql .= $key .' = '. $value .' AND ';  //selain itu tambahkan AND dibelakang field
                }
            }
        }

        // check if groupb is not empty
        // check if where is not empty
        if(sizeof($groupby) > 0) 
        {
            $sql .=' GROUP BY ';
            foreach($groupby as $key => $value)
            {
                if($key == sizeof($groupby) - 1)    // kalau cuma 1 atau data terakhir,
                {                                   //  langsung buat query wherenya
                    $sql .= $value;
                }
                else
                {
                    $sql .=$value .', ';  //selain itu tambahkan AND dibelakang field
                }
            }
        }
    
        return $this->db->query($sql)->result();
    }

    protected function find_all_order_limit($table_name = null, $order = null, $limit = 0) {
        return $this->db->from($table_name)->order_by($order, 'DESC')->limit($limit)->get()->result();
    }

    /* */
    protected function find_all_limit($table_name = null, $limit = 0, $offset = 10) {

    }

    /* */
    protected function find_all_by_criteria($table_name = null, $data = array(), $limit = null, $offset = null) {
        
    }

    /* */
    public function find_by_id($table_name = null, $id = 0) 
    {
        $this->db->from($table_name);
        $this->db->where($this->id, $id);
        $query = $this->db->get();
        return $query->row();
    }

    public function find_by($table_name = null, $key = null, $value = null)
    {
        $this->db->from($table_name);
        $this->db->where($key, $value);
        $query = $this->db->get();
        return $query->row();
    }


    public function add_log($table = null, $data_id = null, $action = null, $newVal = array(), $oldVal = null)
    {
        $value = $newVal;

        if(!is_null($oldVal))   // kalau object ga null, convert ke array
        $oldVal = (array) $oldVal;

        if( (array) $oldVal)    // check oldVal nya ada isinya apa ga
            $value = array_diff($newVal, $oldVal);

        $datas = array(
            'data_id'   => $data_id,
            'action'    => $action,
            'value'     => json_encode($value),
            'created_by'=> $_SESSION['identity'],
            'created'   => date('Y-m-d H:i:s', now())
        );

        if($this->db->insert($table.'_log', $datas))
            return true;
        else
            return false;
    }

    
    /**
     * get user id by username
     * kondisi :
     * 1. kalau dusebutkan usernamenya, maka by username.
     * 2. kalau username null, maka ambil dari session login
     */
    public function get_user_id($username)
    {
        $result = null;
        if(null == $username) {
            $result = $this->find_specific_field_where('tuser', 'id', array('username' => $this->session->userdata('username')));
        }
        else {
            $result = $this->find_specific_field_where('tuser', 'id', array('username' => $username));
        }

        if(count($result) > 0)
            return $result[0]->id;
        else   
            return 0;
    }

    /**
     * fungsi save or update yang perlu record user pelaku
     */
    public function save_or_update($table_name, $data = null, $id = false, $specified_id = null) 
    {
        $date = new DateTime();

        if($id === false) {
            $data['created_by'] = $this->get_user_id(null);
            $data['created'] = $date->format('Y-m-d H:i:s');
            $data['modified'] = $date->format('Y-m-d H:i:s');
            $this->db->insert($table_name, $data);

            return true;

        } else {
            $data['modified_by'] = $this->get_user_id(null);
            $data['modified'] = $date->format('Y-m-d H:i:s');

            if($specified_id != null)
                $this->db->where($specified_id, $id);
            else
                $this->db->where($this->id, $id);

            $this->db->update($table_name, $data);

            return true;
        }

        return false;
    }


    public function save($table_name, $data = null, $id = false) 
    {
        if($id === false) {
            $this->db->insert($table_name, $data);
            return true;
        } else {
            $existingData = $this->find_by($table_name, 'id', $id);
            
            $this->db->where($this->id, $id);
            $this->db->update($table_name, $data);
            return true;
        }
        return false;
    }

    public function soft_delete($label, $table_name, $id)
    {
        $datas = array('is_deleted' => true);

        $this->db->where($this->id, $id);
        if($this->db->update($table_name, $datas))
        {
            // set logger delete
            $this->model->add_log($this->table, $id, DATA_DELETE, $datas, null);
            
            return true;
        }
        else
        {
            log_message('error', 'Error soft delete'.$label.' : '. $this->db->error());
            return false;
        }
    }

    function delete($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}



    /**
     * 
     */
    private function _get_datatables_query($table_name = null, 
                                            $column_search = array(),
                                            $column_order = array(),
                                            $search_value = null,
                                            $order_0_column = null,
                                            $order_0_dir = null,
                                            $order = null,
                                            $where = array(),
                                            $whereIn = false
                                            )
    {
         
        $this->db->from($table_name);

        if($whereIn){
            foreach( $where as $key => $value){
                $this->db->where_in($key, $value);
            }
        }
        else{
            foreach( $where as $key => $value){
                $this->db->where($key, $value);
            }
        }
 
        $i = 0;
     
        foreach ($column_search as $item) // looping awal
        {
            if($search_value) // jika datatable mengirimkan pencarian dengan metode POST
            {
                 
                if($i===0) // looping awal
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $search_value);
                }
                else
                {
                    $this->db->or_like($item, $search_value);
                }
 
                if(count($column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
        }
         
        if(isset($order) && $order != 'kode') 
        {
            $this->db->order_by($column_order[$order_0_column], $order_0_dir);
        } 
        else if(isset($order) && $order == 'kode') 
        {
            $order = $this->order_kode;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    public function get_datatables($table_name, $column_search, $column_order, $search_value, $order_0_column, $order_0_dir, $order, $length = 0, $start = 0, $where = array() , $whereIn = false)
    {

        $this->_get_datatables_query($table_name, $column_search, $column_order, $search_value, $order_0_column, $order_0_dir, $order, $where , $whereIn);
        if($length != -1)
        $this->db->limit($length, $start);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function count_filtered($table_name, $column_search, $column_order, $search_value, $order_0_column, $order_0_dir, $order, $length = 0, $start = 0, $where = array(), $whereIn = false)
    {
        $this->_get_datatables_query($table_name, $column_search, $column_order, $search_value, $order_0_column, $order_0_dir, $order, $where , $whereIn);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
 
    public function count_all($table)
    {
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    public function count_all_where($table, $where = array()){
        
        $this->db->from($table);

        foreach ($where as $key => $value)
        {
            $this->db->where($key, $value);
        }

        return $this->db->count_all_results();
    }


    function count_by_table_and_attribute($table_name = null, $fields = array(), $condition = array())
    {
        $query = $this->db->select('distinct COUNT('.$fields.') AS total');

        foreach ( (array) $condition as $key => $value)
        {
            $query = $this->db->where($key, $value);
        }

        $query = $this->db->get($table_name);

        // return $query->get_compiled_select($table_name);

        return $query->row();
        
    }

    function count_by_table_and_attribute_in($table_name = null, $fields = array(), $condition = array(), $in = array())
    {
        $query = $this->db->select('distinct COUNT('.$fields.') AS total');

        foreach ( (array) $condition as $key => $value)
        {
            $query = $this->db->where($key, $value);
        }

        foreach ( (array) $in as $key => $value)
        {
            $query = $this->db->where_in($key, $value);
        }

        // echo  $query->get_compiled_select($table_name);
        $query = $this->db->get($table_name);
        return $query->row();
        
    }

    function distinct_by_table_and_attribute($table_name = null, $fields = array(), $condition = array())
    {
        $query = $this->db->select('distinct ('.$fields.') AS total');

        foreach ( (array) $condition as $key => $value)
        {
            $query = $this->db->where($key, $value);
        }

        $query = $this->db->get($table_name);

        return (object)  array('total' => sizeof($query->result()));
        
    }

    function distinct_by_table_and_attribute_in($table_name = null, $fields = array(), $condition = array(), $in = array())
    {
        $query = $this->db->select('distinct ('.$fields.') AS total');

        foreach ( (array) $condition as $key => $value)
        {
            $query = $this->db->where($key, $value);
        }

        foreach ( (array) $in as $key => $value)
        {
            $query = $this->db->where_in($key, $value);
        }

        // echo  $query->get_compiled_select($table_name);
        $query = $this->db->get($table_name);
        return (object)  array('total' => sizeof($query->result()));
        
    }

    function execute_query($strQuery)
    {    
        $query = $this->db->query($strQuery);
        return $query->result_array();
    }



}