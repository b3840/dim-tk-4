
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h5 >Data Supplier</h5>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Data Supplier</li>
                </ol>
            </div>
        </div>
    </div>
</div>


<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 ">
                <div class="card">
                    <div class="card-body">
                    <a class="btn btn-info" href="<?php echo base_url()?>supplier/form"><i class="fas fa-plus"></i> Tambah Data</a> <br/> <br/>
                        <table id="tbl" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td>ID</td>
                                    <td>Nama</td>
                                    <td>Alamat</td>
                                    <td>Kota</td>
                                    <td>Provinsi</td>
                                    <td>Telepon</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

        
    <script>
        var table;
        $(document).ready(function() {
        table = $('#tbl').DataTable({ 
                "processing": true, 
                "serverSide": true, 
                "order": [], 
                "autoWidth": false,
                "responsive": true,
                "pageLength": 25,
                "ajax": {
                    "url": "<?php echo site_url('supplier/get_data')?>",
                    "type": "POST",
                },
                "columnDefs": [
                    { 
                        "targets": [ 0 ], 
                        "orderable": false, 
                    },
                ],
            });
        });

        var openLaporan = function (url){
            window.location.href = url;
        }

        function confirmDelete(url){
            var a =  confirm("Apakah anda yakin akan menghapus data ?")
            if(a) {window.location.href = url;}
        }
    
    </script>
</section>
