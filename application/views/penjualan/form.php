
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h5 >Data Penjualan</h5>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Data Penjualan</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 ">
                <div class="card card-info">
                    <div class="card-header">
                        <h4 class="card-title">Penjualan</h4>
                    </div>
                     <div id="infoMessage" style="color:red;padding:5px"><?php echo $message;?></div>
                    <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post" id="frmPenjualan">
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="tanggal_pesan" class="col-sm-2 col-form-label">Tanggal</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control"  value="<?php if(isset($existingData->tanggal)) echo $existingData->tanggal?>"  id="tanggal"  name="tanggal" placeholder="yyyy-mm-dd" data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy-mm-dd" data-mask>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="tanggal_pesan" class="col-sm-2 col-form-label">Pelanggan</label>
                                <div class="col-sm-10">
                                    <select class="form-control select2" name="id_pelanggan" id="id_pelanggan" >
                                    <?php foreach($customers as $customer): ?>
                                            <option value="<?php echo $customer->id; ?>" <?php if (isset($existingData->id_pelanggan)) { if ($existingData->id_pelanggan == $customer->id ) echo 'selected="selected"' ;}  ?> ><?php echo $customer->nama;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="id" class="col-sm-2 col-form-label">ID Barang</label>
                                <div class="col-sm-10">
                                    <select class="form-control select2" style="width: 100%;" name="id_barang" id="id_barang" >
                                        <?php foreach($barangs as $barang): ?>
                                            <option value="<?php echo $barang->id; ?>" <?php if (isset($existingData->id_barang)) { if ($existingData->id_barang == $barang->id ) echo 'selected="selected"' ;}  ?> >[<?php echo $barang->id;?>] <?php echo $barang->nama;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="nama" class="col-sm-2 col-form-label">Nama Barang</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" readonly value="<?php if(isset($existingData->nama_barang)) echo $existingData->nama_barang?>" id="nama_barang" name="nama_barang" placeholder="Nama Barang">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="harga_beli" class="col-sm-2 col-form-label">Harga Beli</label>
                                <div class="col-sm-10">
                                <input type="number" class="form-control" readonly value="<?php if(isset($existingData->harga_beli)) echo $existingData->harga_beli?>" id="harga_beli"  name="harga_beli" placeholder="0">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lead_time" class="col-sm-2 col-form-label">Stok</label>
                                <div class="col-sm-10">
                                <input type="number" class="form-control" readonly value="<?php if(isset($existingData->jumlah)) echo $existingData->jumlah?>" id="jumlah" name="jumlah" placeholder="0">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="lead_time" class="col-sm-2 col-form-label">Jumlah Beli</label>
                                <div class="col-sm-10">
                                <input type="number" class="form-control"  value="<?php if(isset($existingData->jumlah_beli)) echo $existingData->jumlah_beli; else echo "0"; ?>" id="jumlah_beli" name="jumlah_beli" placeholder="0">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="lead_time" class="col-sm-2 col-form-label">Total Harga</label>
                                <div class="col-sm-10">
                                <input type="number" readonly class="form-control"  value="<?php if(isset($existingData->total_harga)) echo $existingData->total_harga  ?>" id="total_harga" name="total_harga" placeholder="0">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="lead_time" class="col-sm-2 col-form-label">Total Stok</label>
                                <div class="col-sm-10">
                                <input type="number" readonly class="form-control" id="total_stok" name="total_stok" placeholder="0">
                                </div>
                            </div>

                        </div>

                        <div class="card-footer">
                            <button type="button" onclick="submitForm()" class="btn btn-info">Simpan</button>
                            <a href="<?php echo base_url();?>penjualan" class="btn btn-default">Batal</a>
                        </div>
                    
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>

        $( document ).ready(function() {
            $('#tanggal').inputmask('yyyy-mm-dd', { 'placeholder': 'yyyy-mm-dd' });

            $('.select2').select2();

            $('#id_barang').on('change', function() {
                getInfoBarang(this.value );
            });

            function getInfoBarang(idBarang)
            {
                $.getJSON("<?php echo site_url('penjualan/get_data_barang/')?>" + idBarang, function(result){
                    if (result)
                    {
                        $('#nama_barang').val(result.nama);
                        $('#harga_beli').val(result.harga_beli);
                        $('#jumlah').val(result.jumlah);
                        //hitungTotal();
                    }
                });
            }
            getInfoBarang($('#id_barang').val());

            $("#jumlah_beli").change(function(){
                hitungTotal();
            });

            setTimeout(function(){
                $('#total_stok').val($('#jumlah').val());
            },2000);

           
        });
        
        function hitungTotal()
        {
            var harga_beli = $('#harga_beli').val();
            if (isNaN(harga_beli)){
                alert("Harga Beli kosong atau bukan angka");
                return false;
            }
            
            var jumlah_beli = $('#jumlah_beli').val();
            if (isNaN(harga_beli)){
                alert("Jumlah Beli kosong atau bukan angka");
                return false;
            }
            var total_harga = Number(harga_beli) * Number(jumlah_beli) ; 

            var stok = $('#jumlah').val();
            if (isNaN(stok)){
                alert("Stok kosong atau bukan angka");
                return false;
            }

            var total_stok = Number(stok) - Number(jumlah_beli);
         
            $('#total_harga').val(total_harga);
            $('#total_stok').val(total_stok);

            return true;
        }

        function submitForm()
        {
            if (hitungTotal()){
                setTimeout(function(){
                    $( "#frmPenjualan" ).submit();
                },1000);
            }
        }
    </script>
</section>