<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Sales | Log in</title>

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
   <link rel="stylesheet" href="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/Adminlte3.1.0/dist/css/adminlte.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/login.css">

</head>
<body class="hold-transition login-page backgroundFull">
<div class="login-box">

  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">

      <p class="login-box-msg" style="font-size:larger"><b>Stock Monitoring</b> With EOQ</p>

      <?php if(isset($message)) {?>
        <div id="infoMessage"><?php echo $message;?></div>
      <?php } ?>

      <form action="<?php echo base_url("login");?>" method="post">
        <div class="input-group mb-3">
          <input type="email" class="form-control" placeholder="Email" id="username" name="username">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Kata kunci" id="password" name="password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                Ingat saya
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Masuk</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <p class="mb-1">
        <a href="#">Lupa password</a>
      </p>
      <p class="mb-0">
        <a href="#" class="text-center">Buat akun baru</a>
      </p>

      <div style="margin-top:50px">
      &#9432; TK4 oleh Kelompok 2 - DHBA - BINUS SI DIM
      </div>
    </div>
  </div>
</div>

<script src="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/jquery/jquery.js"></script>
<script src="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url();?>assets/Adminlte3.1.0/dist/js/adminlte.js"></script>

</body>
</html>
