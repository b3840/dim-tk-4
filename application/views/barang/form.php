
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h5 >Data Barang</h5>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Data Barang</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 ">
                <div class="card card-info">
                    <div class="card-header">
                        <h4 class="card-title">Barang</h4>
                    </div>
                     <div id="infoMessage" style="color:red;padding:5px"><?php echo $message;?></div>
                    <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post">
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="id" class="col-sm-2 col-form-label">ID</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" value="<?php if(isset($existingData->id)) echo $existingData->id?>" id="id" name="id" placeholder="SKU-000">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" value="<?php if(isset($existingData->nama)) echo $existingData->nama?>" id="nama" name="nama" placeholder="Nama Barang">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="harga_beli" class="col-sm-2 col-form-label">Harga Beli</label>
                                <div class="col-sm-10">
                                <input type="number" class="form-control" value="<?php if(isset($existingData->harga_beli)) echo $existingData->harga_beli?>" id="harga_beli"  name="harga_beli" placeholder="0">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="harga_jual" class="col-sm-2 col-form-label">Harga Jual</label>
                                <div class="col-sm-10">
                                <input type="number" class="form-control" value="<?php if(isset($existingData->harga_jual)) echo $existingData->harga_jual?>" id="harga_jual" name="harga_jual" placeholder="0">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="jumlah" class="col-sm-2 col-form-label">Jumlah</label>
                                <div class="col-sm-10">
                                <input type="number" class="form-control" value="<?php if(isset($existingData->jumlah)) echo $existingData->jumlah?>" id="jumlah"  name="jumlah" placeholder="0">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="biaya_pesan" class="col-sm-2 col-form-label">Biaya Pesan</label>
                                <div class="col-sm-10">
                                <input type="number" class="form-control" value="<?php if(isset($existingData->biaya_pesan)) echo $existingData->biaya_pesan?>" id="biaya_pesan" name="biaya_pesan" placeholder="0">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="biaya_simpan" class="col-sm-2 col-form-label">Biaya Simpan</label>
                                <div class="col-sm-10">
                                <input type="number" class="form-control" value="<?php if(isset($existingData->biaya_simpan)) echo $existingData->biaya_simpan?>" id="biaya_simpan" name="biaya_simpan" placeholder="0">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lead_time" class="col-sm-2 col-form-label">Lead Time</label>
                                <div class="col-sm-10">
                                <input type="number" class="form-control" value="<?php if(isset($existingData->lead_time)) echo $existingData->lead_time?>" id="lead_time" name="lead_time" placeholder="0">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="tanggal_pesan" class="col-sm-2 col-form-label">Tanggal Pesan</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control"  value="<?php if(isset($existingData->tanggal_pesan)) echo $existingData->tanggal_pesan?>"  id="tanggal_pesan"  name="tanggal_pesan" data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy-mm-dd" data-mask>
                                </div>
                            </div>
                        
                        </div>

                        <div class="card-footer">
                            <button type="submit" class="btn btn-info">Simpan</button>
                            <a href="<?php echo base_url();?>barang" class="btn btn-default">Batal</a>
                        </div>
                    
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
         $('#tanggal_pesan').inputmask('yyyy-mm-dd', { 'placeholder': 'yyyy-mm-dd' });
    </script>
</section>