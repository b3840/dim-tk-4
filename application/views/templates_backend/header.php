<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Stock Monitoring</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  

  <link rel="stylesheet" href="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/summernote/summernote-bs4.min.css">

  <link rel="stylesheet" href="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/icheck-bootstrap/icheck-bootstrap.min.css">

  <link rel="stylesheet" href="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

  <link rel="stylesheet" href="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

  <link rel="stylesheet" href="<?php echo base_url();?>assets/Adminlte3.1.0/dist/css/adminlte.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/Adminlte3.1.0/dist/css/style.css">


  <script src="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/jquery/jquery.js"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <script src="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/chart.js/Chart.bundle.js"></script>
  <script src="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/jquery-ui/jquery-ui.min.js"></script>
  <script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>

  <script src="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/chart.js/Chart.min.js"></script>
  <script src="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/sparklines/sparkline.js"></script>
  <script src="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/jquery-knob/jquery.knob.min.js"></script>
  <script src="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/moment/moment.min.js"></script>
  <script src="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/daterangepicker/daterangepicker.js"></script>
  <script src="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
  <script src="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/summernote/summernote-bs4.min.js"></script>
  <script src="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
  
  <script src="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
  <script src="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
  <script src="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
  <script src="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
  <script src="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
  <script src="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/datatables-buttons/js/buttons.print.min.js"></script>
  <script src="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
  <script src="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/moment/moment.min.js"></script>
  <script src="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/inputmask/jquery.inputmask.min.js"></script>
  <script src="<?php echo base_url();?>assets/Adminlte3.1.0/plugins/select2/js/select2.full.min.js"></script>

  <script src="<?php echo base_url();?>assets/Adminlte3.1.0/dist/js/adminlte.js"></script>


</head>
<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

    <div class="preloader flex-column justify-content-center align-items-center">
      <img class="animation__shake" src="<?php echo base_url();?>assets/img/logo.png" alt="TELogo" height="80" width="80">
    </div>

    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
      </ul>

      <ul class="navbar-nav ml-auto">
        <!-- Navbar Search -->
        <li class="nav-item">
          <a class="nav-link" data-widget="navbar-search" href="#" role="button">
            <i class="fas fa-search"></i>
          </a>
          <div class="navbar-search-block">
            <form class="form-inline">
              <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                  <button class="btn btn-navbar" type="submit">
                    <i class="fas fa-search"></i>
                  </button>
                  <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </li>

    
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-bell"></i>
            <span class="badge badge-warning navbar-badge">15</span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <span class="dropdown-item dropdown-header">15 Notifications</span>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-envelope mr-2"></i> 4 new messages
              <span class="float-right text-muted text-sm">3 mins</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-users mr-2"></i> 8 friend requests
              <span class="float-right text-muted text-sm">12 hours</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-file mr-2"></i> 3 new reports
              <span class="float-right text-muted text-sm">2 days</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-widget="fullscreen" href="#" role="button">
            <i class="fas fa-expand-arrows-alt"></i>
          </a>
        </li>
      </ul>
    </nav>




