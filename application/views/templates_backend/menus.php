<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="<?php echo base_url();?>assets/img/logo.png" alt="Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Stock Monitoring</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo base_url();?>assets/img/avatar.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php if (isset($userInfo->nama_depan)) echo $userInfo->nama_depan . ' ' . $userInfo->nama_belakang   ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <?php if ( $userInfo->level == 1) { ?>
          <li class="nav-item">
            <a  href="<?php echo base_url();?>admin/users" class="nav-link">
              <i class="fas fa-user"></i>
              <p>Pengguna</p>
            </a>
          </li>
          <?php }   ?>


          <li class="nav-item">
            <a  href="<?php echo base_url();?>dashboard" class="nav-link">
              <i class="fas fa-user"></i>
              <p>Dashboard</p>
            </a>
          </li>

          <li class="nav-item menu-open" >
              <a href="#" class="nav-link">
              <i class="fas fa-chevron-right"></i>
                <p> Data Master <i class="fas fa-angle-left right"></i></p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?php echo base_url();?>barang" class="nav-link">
                    <i class="fas fa-box-open"></i>
                    <p>Barang</p>
                  </a>
                </li>
              </ul>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                <a href="<?php echo base_url();?>customer" class="nav-link">
                  <i class="fas fa-users"></i>
                    <p>Pelanggan</p>
                  </a>
                </li>
              </ul>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?php echo base_url();?>supplier" class="nav-link">
                  <i class="fas fa-address-book"></i>
                    <p>Supplier</p>
                  </a>
                </li>
              </ul>
            </li>

            <li class="nav-item menu-open" >
              <a href="#" class="nav-link">
              <i class="fas fa-chevron-right"></i>
                <p> Transaksi <i class="fas fa-angle-left right"></i></p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?php echo base_url();?>pembelian"  class="nav-link">
                  <i class="fas fa-dollar-sign"></i>
                    <p>Pembelian</p>
                  </a>
                </li>
              </ul>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?php echo base_url();?>penjualan"  class="nav-link">
                  <i class="fas fa-hand-holding-usd"></i>
                    <p>Penjualan</p>
                  </a>
                </li>
              </ul>
            </li>

            <li class="nav-item menu-open" >
              <a href="#" class="nav-link">
                <i class="fas fa-chevron-right"></i>
                <p> Perhitungan <i class="fas fa-angle-left right"></i></p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?php echo base_url();?>eoq" class="nav-link">
                  <i class="fas fa-square-root-alt"></i>
                    <p>Perhitungan EOQ</p>
                  </a>
                </li>
              </ul>
            </li>

            <li class="nav-item">
              <a  href="<?php echo base_url();?>admin/logout" class="nav-link">
              <i class="fas fa-sign-out-alt"></i>
                <p>Log Out</p>
              </a>
            </li>
        </ul>
      </nav>
    </div>
  </aside>