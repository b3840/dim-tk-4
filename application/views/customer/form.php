
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h5 >Data Pelanggan</h5>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Data Pelanggan</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 ">
                <div class="card card-info">
                    <div class="card-header">
                        <h4 class="card-title">Pelanggan</h4>
                    </div>
                     <div id="infoMessage" style="color:red;padding:5px"><?php echo $message;?></div>
                    <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post">
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" value="<?php if(isset($existingData->nama)) echo $existingData->nama?>" id="nama" name="nama" placeholder="Nama">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="harga_beli" class="col-sm-2 col-form-label">Alamat</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" value="<?php if(isset($existingData->alamat)) echo $existingData->alamat?>" id="alamat"  name="alamat" placeholder="Alamat">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="harga_jual" class="col-sm-2 col-form-label">Kota</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" value="<?php if(isset($existingData->kota)) echo $existingData->kota?>" id="kota" name="kota" placeholder="Kota">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="harga_jual" class="col-sm-2 col-form-label">Provinsi</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" value="<?php if(isset($existingData->provinsi)) echo $existingData->provinsi?>" id="provinsi" name="provinsi" placeholder="Provinsi">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="jumlah" class="col-sm-2 col-form-label">Telepon</label>
                                <div class="col-sm-10">
                                <input type="number" class="form-control" value="<?php if(isset($existingData->telepon)) echo $existingData->telepon?>" id="telepon"  name="telepon" placeholder="Telepon">
                                </div>
                            </div>
                        </div>

                        <div class="card-footer">
                            <button type="submit" class="btn btn-info">Simpan</button>
                            <a href="<?php echo base_url();?>customer" class="btn btn-default">Batal</a>
                        </div>
                    
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        
    </script>
</section>