<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Eoq extends MY_Controller 
{
    public function __construct()
	{
        parent::__construct();

		$this->load->helper('url');
		$this->load->helper('form');

		$this->table = 'eoq';
        $this->id = "id";

        $this->tableBarang = "barang";

		$this->load->helper('url');
		$this->load->helper('form');

        $this->load->model('MY_model', 'model');
        
        $this->column_order = array();
        $this->column_search = array();
    }

    public function chkUser(){
		if (($this->session->userdata('username') == null))
		{
			redirect('/login');
		}
		else
		{
			$userx = $this->session->userdata('username');
			$userInfo = $this->model->find_by("tuser", "username", $userx);
			if($userInfo == null)
			{
				redirect('/login');
			}
		}
	}

    public function index()
	{
    	$this->chkUser();

		$userx = $this->session->userdata('username');
		$userInfo = $this->model->find_by("tuser", "username", $userx);

		$this->data['userInfo'] = $userInfo;
        $this->data['content'] = 'eoq/index';
        $this->load->view('template_backend', $this->data);
	}

    public function form()
    {
        $this->chkUser();

        $user =  $this->session->userdata('username');

        $this->data['userx'] = $user;
		
		$userInfo = $this->model->find_by("tuser", "username", $this->data['userx']);

        $this->data['userInfo'] = $userInfo;

        $barangs = $this->model->find_all_asc($this->id, $this->tableBarang);

        $this->data['barangs'] = $barangs;

		$id = urldecode($this->uri->segment(3));

		if($id == null)
        {
            $this->data['ids'] = null;
        } 
        else 
        {
            $this->data['ids'] = $id;
		}

        if($this->data['ids']  == null) 
        {
             //insert new
            if ($this->input->post())
            {
                $datas = array (
					'id' 	        => $this->input->post('id'),
                    'tanggal'          => $this->input->post('tanggal'),
				    'bulan'    => $this->input->post('bulan'),
                    'id_barang' 	=> $this->input->post('id_barang'),
                    'nama_barang' 	    => $this->input->post('nama_barang'),
                    'harga_beli'   => $this->input->post('harga_beli'),
                    'harga_pesan'  => $this->input->post('harga_pesan'),
                    'biaya_pesan'     => $this->input->post('biaya_pesan'),
                    'biaya_simpan' => $this->input->post('biaya_simpan'),
                    'lead_time' => $this->input->post('lead_time'),
                    'permintaan' => $this->input->post('permintaan'),
                    'eoq' => $this->input->post('eoq'),
                    // 'rop' => $this->input->post('rop'),
                    // 'total_biaya' => $this->input->post('total_biaya')
                );

                if($this->model->save($this->table, $datas)) 
				{
                  	$this->session->set_flashdata('message', 'Berhasil simpan data .');
                    redirect('eoq', 'refresh');
				}
				else
				{
					$this->session->set_flashdata('message', 'Gagal simpan data .');
                    redirect(current_url(), 'refresh');
				}
            }
        }
        else 
        {
            //update
            $existingData = $this->model->find_by($this->table, $this->id, $this->data['ids']);
            $this->data['existingData'] = $existingData;

            if(!isset($this->data['existingData']->id))
            {
                redirect(site_url()."eoq");
            }

            if ($this->input->post())
			{
                $datas = array (
                    'tanggal'          => $this->input->post('tanggal'),
				    'bulan'    => $this->input->post('bulan'),
                    'id_barang' 	=> $this->input->post('id_barang'),
                    'nama_barang' 	    => $this->input->post('nama_barang'),
                    'harga_beli'   => $this->input->post('harga_beli'),
                    'harga_pesan'  => $this->input->post('harga_pesan'),
                    'biaya_pesan'     => $this->input->post('biaya_pesan'),
                    'biaya_simpan' => $this->input->post('biaya_simpan'),
                    'lead_time' => $this->input->post('lead_time'),
                    'permintaan' => $this->input->post('permintaan'),
                    'eoq' => $this->input->post('eoq'),
                    // 'rop' => $this->input->post('rop'),
                    // 'total_biaya' => $this->input->post('total_biaya')
                );

                if($this->model->save($this->table, $datas, $this->data['ids'])) 
                {
                 	$this->session->set_flashdata('message', 'Berhasil simpan data ');
                    redirect(current_url(), 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('message', 'Gagal simpan data .');
                    redirect(current_url(), 'refresh');
                }
            }
        }
    
        $this->data['message'] = $this->session->flashdata('message');
        $this->data['content'] = 'eoq/form';   
        $this->load->view('template_backend', $this->data);
    }

    public function get_data_barang()
    {
        $id = urldecode($this->uri->segment(3));
        $userInfo = $this->model->find_by($this->tableBarang, $this->id, $id);
        echo json_encode($userInfo);
    }

    public function getMonthNameIndonesia($mnt)
    {
        $montName = "Januari";
        if ($mnt == 1)  $montName = "Januari";
        else if ($mnt == 2)  $montName = "Februari";
        else if ($mnt == 3)  $montName = "Maret";
        else if ($mnt == 4)  $montName = "April";
        else if ($mnt == 5)  $montName = "Mei";
        else if ($mnt == 6)  $montName = "Juni";
        else if ($mnt == 7)  $montName = "Juli";
        else if ($mnt == 8)  $montName = "Agustus";
        else if ($mnt == 9)  $montName = "September";
        else if ($mnt == 10)  $montName = "Oktober";
        else if ($mnt == 11)  $montName = "November";
        else if ($mnt == 12)  $montName = "Desember";

        return $montName;
    }

    public function get_data()
    {
        if(isset($_POST)) {

            $this->data['userx']  = $this->session->userdata('username');
            $userInfo = $this->model->find_by("tuser", "username", $this->data['userx']);

            $order_column = isset($_POST['order']['0']['column']) ? $_POST['order']['0']['column'] : 1;
            $order_dir = isset($_POST['order']['0']['dir']) ? $_POST['order']['0']['dir'] : 'asc';
            $order = isset($_POST['order']) ? $_POST['order'] : null;
            $list_data = $this->model->get_datatables($this->table,
                                    $this->column_search,
                                    $this->column_order,
                                    $_POST['search']['value'], 
                                    $order_column, 
                                    $order_dir,
                                    $order,
                                    $_POST['length'],
                                    $_POST['start']
                                );

            $list = $list_data;
            $data = array();
            $no   = $_POST['start'] != null ? $_POST['start'] : 0;
            foreach ($list as $field) {	
 
                $no = $no + 1;
                $menus = "<a class='btn btn-sm btn-warning' href='".site_url()."eoq/form/".$field->id."' data-toggle='tooltip' data-placement='top' title='". lang('toggle_edit')."'><i class='fa fa-edit'></i> </a>&nbsp;";
                $menus .= "<a class='btn btn-sm btn-danger'  onclick='confirmDelete(\"".site_url()."eoq/delete/".$field->id."\")' href='#'  href='#' data-toggle='tooltip' data-placement='top' title='". lang('toggle_delete')."'><i class='fas fa-trash-alt'></i> </a>";
               
                $row = array();
                $row[] = $no;
                $row[] = $field->id;
                $row[] = $field->tanggal;
                $row[] = $this->getMonthNameIndonesia($field->bulan);
                $row[] = $field->id_barang;
                $row[] = $field->nama_barang;
                $row[] = number_format( $field->biaya_pesan , 0 , '.' , ',' ); 
                $row[] = number_format( $field->biaya_simpan , 0 , '.' , ',' ); 
                $row[] = number_format( $field->permintaan , 0 , '.' , ',' ); 
                $row[] = number_format( $field->lead_time , 0 , '.' , ',' ); 
                $row[] = number_format( $field->eoq , 0 , '.' , ',' ); 
				// $row[] = number_format( $field->rop , 0 , '.' , ',' );
				// $row[] = number_format( $field->total_biaya , 0 , '.' , ',' );
                $row[] = $menus;
                $data[] = $row;
            }
			 
			$output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->model->count_all_where($this->table, array()),
                "recordsFiltered" => $this->model->count_filtered($this->table,
                                                                    $this->column_search,
                                                                    $this->column_order,
                                                                    $_POST['search']['value'], 
                                                                    $order_column, 
                                                                    $order_dir,
                                                                    $order,
                                                                    $_POST['length'],
                                                                    $_POST['start']
                                                                ),
                "data" => $data,
            );
            echo json_encode($output);
        }
    }



}