<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan extends MY_Controller 
{
    public function __construct()
	{
        parent::__construct();

		$this->load->helper('url');
		$this->load->helper('form');

		$this->table = 'penjualan';
        $this->id = "id";

        $this->tableBarang = 'barang';
        $this->tablePelanggan = 'customer';

		$this->load->helper('url');
		$this->load->helper('form');

        $this->load->model('MY_model', 'model');
        
        $this->column_order = array();
        $this->column_search = array();
    }

    public function chkUser(){
		if (($this->session->userdata('username') == null))
		{
			redirect('/login');
		}
		else
		{
			$userx = $this->session->userdata('username');
			$userInfo = $this->model->find_by("tuser", "username", $userx);
			if($userInfo == null)
			{
				redirect('/login');
			}
		}
	}

    public function index()
	{
    	$this->chkUser();

		$userx = $this->session->userdata('username');
		$userInfo = $this->model->find_by("tuser", "username", $userx);

		$this->data['userInfo'] = $userInfo;
        $this->data['content'] = 'penjualan/index';
        $this->load->view('template_backend', $this->data);
	}

    public function form()
    {
        $this->chkUser();

        $user =  $this->session->userdata('username');

        $this->data['userx'] = $user;
		
		$userInfo = $this->model->find_by("tuser", "username", $this->data['userx']);

        $this->data['userInfo'] = $userInfo;

        $customers = $this->model->find_all_asc($this->id, $this->tablePelanggan);
        $this->data['customers'] = $customers;

        $barangs = $this->model->find_all_asc($this->id, $this->tableBarang);
        $this->data['barangs'] = $barangs;

		$id = urldecode($this->uri->segment(3));

		if($id == null)
        {
            $this->data['ids'] = null;
        } 
        else 
        {
            $this->data['ids'] = $id;
		}

        if($this->data['ids']  == null) 
        {
             //insert new
            if ($this->input->post())
            {
                $datas = array (
                    'tanggal'       => $this->input->post('tanggal'),
				    'id_pelanggan'   => $this->input->post('id_pelanggan'),
                    'id_barang' 	=> $this->input->post('id_barang'),
                    'jumlah_beli' 	=> $this->input->post('jumlah_beli'),
                    'total_harga'   => $this->input->post('total_harga'),
                );

                $stok = array ('jumlah' => $this->input->post('total_stok'));

                if($this->model->save($this->table, $datas)) 
				{
                    $this->model->save($this->tableBarang, $stok, $this->input->post('id_barang'));
                  	$this->session->set_flashdata('message', 'Berhasil simpan data .');
                    redirect('penjualan', 'refresh');
				}
				else
				{
					$this->session->set_flashdata('message', 'Gagal simpan data .');
                    redirect(current_url(), 'refresh');
				}
            }
        }
        else 
        {
            //update
            $existingData = $this->model->find_by($this->table, $this->id, $this->data['ids']);
            $this->data['existingData'] = $existingData;

            if(!isset($this->data['existingData']->id))
            {
                redirect(site_url()."barang");
            }

            if ($this->input->post())
			{
                $datas = array (
                    'tanggal'       => $this->input->post('tanggal'),
				    'id_pelanggan'  => $this->input->post('id_pelanggan'),
                    'id_barang' 	=> $this->input->post('id_barang'),
                    'jumlah_beli' 	=> $this->input->post('jumlah_beli'),
                    'total_harga'   => $this->input->post('total_harga'),
                );

                $stok = array ('jumlah' => $this->input->post('total_stok'));

                if($this->model->save($this->table, $datas, $this->data['ids'])) 
                {
                    $this->model->save($this->tableBarang, $stok, $this->input->post('id_barang'));
                 	$this->session->set_flashdata('message', 'Berhasil simpan data ');
                    redirect(current_url(), 'refresh');
                }
                else
                {
                    $this->session->set_flashdata('message', 'Gagal simpan data .');
                    redirect(current_url(), 'refresh');
                }
            }
        }
    
        $this->data['message'] = $this->session->flashdata('message');
        $this->data['content'] = 'penjualan/form';   
        $this->load->view('template_backend', $this->data);
    }

    public function delete()
    {
        $id = urldecode($this->uri->segment(3));
		if($id == null)
        {
            redirect(site_url()."penjualan");
        } 
        else 
        {
            $where = array('id' => $id);
            $this->model->delete( $where, $this->table);
            redirect(site_url()."penjualan");
        }
    }

    
    public function get_data_barang()
    {
        $id = urldecode($this->uri->segment(3));
        $barangInfo = $this->model->find_by($this->tableBarang, $this->id, $id);
        echo json_encode($barangInfo);
    }

    public function get_data()
    {
        if(isset($_POST)) {

            $this->data['userx']  = $this->session->userdata('username');
            $userInfo = $this->model->find_by("tuser", "username", $this->data['userx']);

            $order_column = isset($_POST['order']['0']['column']) ? $_POST['order']['0']['column'] : 1;
            $order_dir = isset($_POST['order']['0']['dir']) ? $_POST['order']['0']['dir'] : 'asc';
            $order = isset($_POST['order']) ? $_POST['order'] : null;
            $list_data = $this->model->get_datatables($this->table,
                                    $this->column_search,
                                    $this->column_order,
                                    $_POST['search']['value'], 
                                    $order_column, 
                                    $order_dir,
                                    $order,
                                    $_POST['length'],
                                    $_POST['start']
                                );

            $list = $list_data;
            $data = array();
            $no   = $_POST['start'] != null ? $_POST['start'] : 0;
            foreach ($list as $field) {	

                $customer = $this->model->find_by($this->tablePelanggan, $this->id, $field->id_pelanggan);

                $barang = $this->model->find_by($this->tableBarang, $this->id, $field->id_barang);             
 
                $no = $no + 1;
                $menus = "<a class='btn btn-sm btn-warning' href='".site_url()."penjualan/form/".$field->id."' data-toggle='tooltip' data-placement='top' title='". lang('toggle_edit')."'><i class='fa fa-edit'></i> </a>&nbsp;";
                $menus .= "<a class='btn btn-sm btn-danger'  onclick='confirmDelete(\"".site_url()."penjualan/delete/".$field->id."\")' href='#'  href='#' data-toggle='tooltip' data-placement='top' title='". lang('toggle_delete')."'><i class='fas fa-trash-alt'></i> </a>";
               
                $row = array();
                $row[] = $field->id;
                $row[] = $field->tanggal;
                $row[] = $customer->nama;
                $row[] = $barang->nama;
                $row[] = number_format( $barang->harga_beli , 0 , '.' , ',' );
                $row[] = number_format( $field->jumlah_beli , 0 , '.' , ',' ); 
                $row[] = number_format( $field->total_harga , 0 , '.' , ',' );
                $row[] = $menus;
                $data[] = $row;
            }
			 
			$output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->model->count_all_where($this->table, array()),
                "recordsFiltered" => $this->model->count_filtered($this->table,
                                                                    $this->column_search,
                                                                    $this->column_order,
                                                                    $_POST['search']['value'], 
                                                                    $order_column, 
                                                                    $order_dir,
                                                                    $order,
                                                                    $_POST['length'],
                                                                    $_POST['start']
                                                                ),
                "data" => $data,
            );
            echo json_encode($output);
        }
    }

}