<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller ini mencakup :
 * 	- views/login.php
 * 	- views/user/*
 */
class Admin extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->load->helper('form');

		$this->load->model('USER_model', 'model');

		$this->column_order = array(null, 'username');
        $this->column_search = array('username');
	}

	public function chkUser(){
		if (($this->session->userdata('username') == null))
		{
			redirect('/login');
		}
		else{
			$userx = $this->session->userdata('username');
			$userInfo = $this->model->find_by("tuser", "username", $userx);
			if($userInfo == null)
			{
				redirect('/login');
			}
		}
	}

	public function index()
	{
		$this->chkUser();

        $this->data['content'] = 'dashboard/dashboard';
        $this->load->view('template_backend', $this->data);
	}

	public function login()
	{
		$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

		if (isset($_POST) && !empty($_POST))
		{
			// validasi isian form
			$validation_config = array (
				array(
					'field' => 'username',
					'label' => 'Username',
					'rules' => 'required',
					'errors' => array(
                        'required' => 'Username tidak boleh kosong',
                	),
				),
				array(
					'field' => 'password',
					'label' => 'Password',
					'rules' => 'required',
					'errors' => array(
                        'required' => 'Password tidak boleh kosong',
                	),
				),
			);

			$this->form_validation->set_rules($validation_config);

			if ($this->form_validation->run() === TRUE)
			{

				$is_password_match = $this->model->password_verification(
					$this->input->post('username'), $this->input->post('password')	
				);

				if($is_password_match == 0)
				{
					$this->model->set_flashdata('password salah / tidak diisi');
					redirect('/login');
				}
				else if($is_password_match == 1)
				{
					$userInfo = $this->model->find_by("tuser", "username", $this->input->post('username'));
					if($userInfo == null)
					{
						$this->model->set_flashdata('Pengguna tidak ditemukan');
						redirect('/login');
					}
					else
					{
						$this->model->set_flashdata('');
						$this->model->session_generate($this->input->post('username'), $usercheck[0]->level );
						redirect('/dashboard');
					}
				}
				else if($is_password_match == 2)
				{
					$this->model->set_flashdata('Pengguna tidak ditemukan');
					redirect('/login');
				}
			}
		}
		$this->load->view('/admin/login', $this->data);
	}

	public function logout()
	{
		$this->model->session_destroy();
		redirect('/');
	}

	public function user_index()
	{
		$this->chkUser();

		$this->data['content'] = 'admin/user/index';
        $this->load->view('template_backend', $this->data);
	}

	

	public function get_data_user()
    {
        if(isset($_POST)) {

            $order_column = isset($_POST['order']['0']['column']) ? $_POST['order']['0']['column'] : 1;
            $order_dir = isset($_POST['order']['0']['dir']) ? $_POST['order']['0']['dir'] : 'asc';
            $order = isset($_POST['order']) ? $_POST['order'] : null;
            $list_data = $this->model->get_datatables(
									'tuser'
                                    , $this->column_search
                                    , $this->column_order
                                    , $_POST['search']['value'] 
                                    , $order_column
                                    , $order_dir
                                    , $order
                                    , $_POST['length']
									, $_POST['start']
									, array('is_deleted' => false)
                                );

		$list = $list_data;
        $data = array();
		$no = $_POST['start'];
		
        foreach ($list as $field) {

            // build user action
            $menus = "<a class='btn btn-xs btn-warning' href='".site_url()."admin/user/edit/".$field->id."' data-toggle='tooltip' data-placement='top' title='". lang('toggle_edit')."'><i class='fa fa-edit'></i> </a>";
            if($this->session->userdata('level') == '1')
            {
                $menus = "<a class='btn btn-xs btn-warning' href='".site_url()."admin/user/edit/".$field->id."' data-toggle='tooltip' data-placement='top' title='". lang('toggle_edit')."'><i class='fa fa-edit'></i> </a> | <a class='btn btn-xs btn-danger' onclick='confirmDeactive(\"".site_url()."admin/user/del/".$field->id."\")' href='#' href='#' data-toggle='tooltip' data-placement='top' title='". lang('toggle_delete')."'><i class='fa fa-trash-o'></i></a>";
            }
			$no++;
				$kantor = $this->model->find_by('tkantor', 'id', $field->kantor_id);
                $row = array();
                $row[] = $no;
				$row[] = $field->username;
				$row[] = $field->email;
				$row[] = $kantor->nama;
				$row[] = $field->is_active == 't' ? "<div style='color: blue;'>Aktif</div>" : "<div style='color: orange;'>Tidak</div>";
				// $row[] = $field->is_active;
                $row[] = $menus;

                $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all_where('tuser', array('level' => 2)),
            "recordsFiltered" => $this->model->count_filtered(
																'tuser'
                                                                , $this->column_search
                                                                , $this->column_order
                                                                , $_POST['search']['value'] 
                                                                , $order_column
                                                                , $order_dir
                                                                , $order
                                                                , $_POST['length']
																, $_POST['start']
																, array('is_deleted' => false)
															),
            "data" => $data,
        );

        //output dalam format JSON
        echo json_encode($output);

        }
    }	
}
