<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->load->helper('form');

		$this->load->model('USER_model', 'model');

		$this->column_order = array(null, 'username');
        $this->column_search = array('username');
	}

	public function chkUser(){
		if (($this->session->userdata('username') == null))
		{
			redirect('/login');
		}
		else
		{
			$userx = $this->session->userdata('username');
			$userInfo = $this->model->find_by("tuser", "username", $userx);
			if($userInfo == null)
			{
				redirect('/login');
			}
		}
	}


    public function index()
	{
    	$this->chkUser();

		$userx = $this->session->userdata('username');
		$userInfo = $this->model->find_by("tuser", "username", $userx);

		$this->data['userInfo'] = $userInfo;
        $this->data['content'] = 'dashboard/index';
        $this->load->view('template_backend', $this->data);
	}

}
